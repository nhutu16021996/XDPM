﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Websach
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
              routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

             routes.MapRoute(
                 name: "Daily",
                 url: "dai-ly/{action}/{id}",
                 defaults: new { controller = "Daily", action = "Index", id = UrlParameter.Optional }
             );
          
              routes.MapRoute(
                 name: "NXB",
                 url: "nha-xuat-ban/{action}/{id}",
                 defaults: new { controller = "NXB", action = "Index", id = UrlParameter.Optional }
             );
            routes.MapRoute(
               name: "Nhapsach",
               url: "nhap-sach/{action}/{id}",
               defaults: new { controller = "Nhapsach", action = "Index", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "chitietnhap",
                url: "nhap-sach/{action}/{id}",
                defaults: new { controller = "Nhapsach", action = "CreateDetail", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "Xuatsach",
               url: "xuat-sach/{action}/{id}",
               defaults: new { controller = "Xuatsach", action = "Index", id = UrlParameter.Optional }
           );
            routes.MapRoute(
               "Sửa chi tiết nhập",
               "Nhapsach/EditDetail/{maphieu}/{masach}",
               new
               {
                   controller = "Nhapsach",
                   action = "EditDetail",
               }
           );
            routes.MapRoute(
               "Sửa chi tiết xuất",
               "Xuatsach/EditDetail/{maphieu}/{masach}",
               new
               {
                   controller = "Xuatsach",
                   action = "EditDetail",
               }
           );
            routes.MapRoute(
               "load thu chi",
               "Thongke/LoadThu_Chi/{start}/{end}",
               new
               {
                   controller = "Thongke",
                   action = "LoadThu_Chi",
                  
               }
           );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            

        }
    }
}