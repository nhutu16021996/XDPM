﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Websach.Models;

namespace Websach.DTO
{
    public class ThongkeDTO
    {
        QLSachEntities db = null;
        public ThongkeDTO()
        {
            db = new QLSachEntities();
        }

        public List<TonkhoTK> Tonkho(DateTime ngay)
        {
          //  db.Configuration.ProxyCreationEnabled = false;
            var model = (from a in db.Saches
                        join b in db.Tonkhoes
                        on a.masach equals b.masach
                        where b.ngayupdatetime <= ngay
                        select new TonkhoTK()
                        {
                            stt = b.stt,
                            masach = a.masach,
                            tensach = a.tensach,
                            soluong = b.soluong,
                            ngayupdatetime = b.ngayupdatetime,
                        }).OrderByDescending(x=>x.ngayupdatetime).ToList();         
            List<int> gotmasach = new List<int>();
            List<TonkhoTK> list= new List<TonkhoTK>();
            foreach (TonkhoTK item in model)
            {
                if (!gotmasach.Contains(item.masach))
                {
                    var i = new TonkhoTK
                    {
                        stt =item.stt,
                        masach = item.masach,
                        tensach = item.tensach,
                        soluong = item.soluong,
                        ngayupdatetime = item.ngayupdatetime,
                    };
                    list.Add(i);
                    gotmasach.Add(item.masach);
                }
            }
            return list;
        }

        public List<CongNoDL> CongNoDL(DateTime ngay)
        {
            var model = (from a in db.Dailies
                         join b in db.CongNo_DL
                         on a.madl equals b.madl
                         where b.ngay <= ngay
                         select new CongNoDL()
                         {
                            madl = b.madl,
                            tendl = a.tendl,
                            ngay = b.ngay,
                            tongno = b.tongno,
                         }).OrderByDescending(x => x.ngay).ToList();
            List<int> gotmadl = new List<int>();
            List<CongNoDL> list = new List<CongNoDL>();
            foreach (CongNoDL item in model)
            {
                if (!gotmadl.Contains(item.madl))
                {
                    var i = new CongNoDL
                    {
                        madl = item.madl,
                        tendl = item.tendl,
                        ngay = item.ngay,
                        tongno = item.tongno,
                    };
                    list.Add(i);
                    gotmadl.Add(item.madl);
                }
            }
            return list;
        }
        public List<CongNoNXB> CongNoNXB(DateTime ngay)
        {
            var model = (from a in db.NXBs
                         join b in db.CongNo_NXB
                         on a.manxb equals b.manxb
                         where b.ngay <= ngay
                         select new CongNoNXB()
                         {
                             manxb = b.manxb,
                             tennxb = a.tennxb,
                             ngay = b.ngay,
                             tongno = b.tongno,
                         }).OrderByDescending(x => x.ngay).ToList();
            List<int> gotmanxb = new List<int>();
            List<CongNoNXB> list = new List<CongNoNXB>();
            foreach (CongNoNXB item in model)
            {
                if (!gotmanxb.Contains(item.manxb))
                {
                    var i = new CongNoNXB
                    {
                        manxb = item.manxb,
                        tennxb = item.tennxb,
                        ngay = item.ngay,
                        tongno = item.tongno,
                    };
                    list.Add(i);
                    gotmanxb.Add(item.manxb);
                }
            }
            return list;
        }


    }
}