﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Websach.Models;

namespace Websach.DTO
{
    public class PhieuThuDTO
    {
         QLSachEntities db = null;
        public PhieuThuDTO()
        {
            db = new QLSachEntities();
        }
        public List<PhieuThu> List()
        {
            return db.PhieuThus.ToList();
        }
        public List<PhieuThu> List(DateTime start ,DateTime end)
        {
            return db.PhieuThus.Where(x=>x.ngaylap>=start &&x.ngaylap <=end).ToList();
        }
        public int Tongthu(DateTime start, DateTime end)
        {
            int tongthu = db.PhieuThus.Where(x => x.ngaylap >= start && x.ngaylap <= end).Sum(x => x.tongthu);
            return tongthu;
        }
        public List<PhieuThu> ListPT_DL(int id)
        {
            return db.PhieuThus.Where(x => x.madl == id).ToList();
        }
        public PhieuThu Details(int id)
        {
            var model = db.PhieuThus.Where(x => x.maphieu == id).SingleOrDefault();
            return model;
        }
        public List<CT_PT> ListDetails(int id)
        {
            var model = db.CT_PT.Where(x => x.mapthu == id).ToList();
            return model;
        }
        public int getNewId()
        {
            var model = db.PhieuThus.Select(x => x.maphieu).ToList().LastOrDefault();
            int NewId = 0;
            if (model == null)
            {
                NewId = 1;
            }
            else { NewId = model + 1; }
            return NewId;
        }
        public string getNameDL(int id)
        {
            var model = db.Dailies.Where(x => x.madl == id).SingleOrDefault();
            return model.tendl;
        }
        public int getTongNo(int id)
        {
            var model = db.Dailies.Where(x => x.madl == id).SingleOrDefault();
            return model.tongno;
        }
        private int updateCongNo(int madl, int tongno, DateTime ngay)
        {
            var modelcongno = db.CongNo_DL.Where(x => x.ngay <= ngay && x.madl == madl).OrderByDescending(x => x.ngay).FirstOrDefault();    
            
            if (modelcongno == null ||modelcongno.tongno - tongno < 0 )
            {
                return 0;
            }
            else
            {
                if (modelcongno.ngay == ngay)
                {
                    modelcongno.tongno -= tongno;
                }
                else
                {
                    CongNo_DL congno = new CongNo_DL();
                    congno.tongno = modelcongno.tongno - tongno;
                    congno.madl = madl;
                    congno.ngay = ngay;
                    db.CongNo_DL.Add(congno);
                }
                
                db.SaveChanges();
                var ngayupdategannhat = db.CongNo_DL.Where(x => x.madl == madl).OrderByDescending(x => x.ngay).FirstOrDefault();
                if (ngayupdategannhat.ngay > ngay)
                {
                    var listupdate = db.CongNo_DL.Where(x => x.madl == madl && x.ngay > ngay).OrderBy(x => x.ngay).ToList();
                    foreach (var i in listupdate)
                    {
                        if (i.tongno - tongno < 0)
                        {
                            return 0;
                        }
                        else
                        {
                            i.tongno -= tongno;
                            
                        }

                    }
                    db.SaveChanges();
                    return 1;
                    
                }
                return 1;
            }
            
        }
        public int getSLTonDL(int masach, int madl,DateTime ngay)
        {
            int tongSLxuat = db.CT_PX.Where(x => x.PhieuXuat.madl == madl && x.masach == masach && x.PhieuXuat.ngaylap <= ngay).Select(x => x.soluong).DefaultIfEmpty().Sum();
            int tongSLthu = db.CT_PT.Where(x => x.PhieuThu.madl == madl && x.masach == masach && x.PhieuThu.ngaylap <= ngay).Select(x => x.soluong).DefaultIfEmpty().Sum();
            int tongTon = tongSLxuat - tongSLthu;
          
            return tongTon;
          
        }
        public int Add(PhieuThu_CT p)
        {
            try
            {
                PhieuThu pn = new PhieuThu { maphieu = p.maphieu, madl = p.madl, ngaylap = p.ngaylap, tongthu = p.tongthu, trangthai = true };
              
                foreach (var i in p.Chitiet)
                {
                   // var tontrcdo = db.Tonkho_DL.Where(x => x.masach == i.masach && x.madl == pn.madl).Select(x => x.soluong).SingleOrDefault();
                    if (getSLTonDL(i.masach, pn.madl, pn.ngaylap) - i.soluong < 0)
                    {
                        return -1;
                    }
                    else
                    {
                        pn.CT_PT.Add(i);
                    }
    
                }
                if (updateCongNo(pn.madl, pn.tongthu, pn.ngaylap)==0)
                {
                    return 0;
                }
                
                db.PhieuThus.Add(pn);
                var dl = db.Dailies.Find(p.madl);
                dl.tongno -= p.tongthu;
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex) { return 0; }
        }
    }
}