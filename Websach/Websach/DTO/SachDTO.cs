﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Websach.Models;


namespace Websach.DTO
{
    public class SachDTO
    {
        QLSachEntities db = null;
        public SachDTO()
        {
            db = new QLSachEntities();
        }

        public List<Sach> List()
        {
            return db.Saches.ToList();
        }
        
        public List<Sach> Sach_NXB(int manxb) 
        {
            return db.Saches.Where(x => x.manxb == manxb).OrderByDescending(x=>x.masach).ToList();
        }
       
        public int getSLTonDL(int masach, int madl)
        {

            int tongSLxuat = db.CT_PX.Where(x => x.PhieuXuat.madl == madl && x.masach == masach).Select(x => x.soluong).DefaultIfEmpty().Sum();
            int tongSLthu = db.CT_PT.Where(x => x.PhieuThu.madl == madl && x.masach == masach).Select(x => x.soluong).DefaultIfEmpty().Sum();
            int tongTon = tongSLxuat - tongSLthu;
            return tongTon;
        }
        public int getSLChuaTra(int masach, int manxb)
        {

            int tongSLnhap = db.CT_PN.Where(x => x.PhieuNhap.manxb == manxb && x.masach == masach).Select(x => x.soluong).DefaultIfEmpty().Sum();
            int tongSLchi = db.CT_PC.Where(x => x.PhieuChi.manxb == manxb && x.masach == masach).Select(x => x.soluong).DefaultIfEmpty().Sum();
            int tongTon = tongSLnhap - tongSLchi;
            return tongTon;
        }
        public List<TonkhoDL> SachTon_DL(int madl)
        {
            var model = db.Saches.ToList();

            List<TonkhoDL> list = new List<TonkhoDL>();
            foreach(Sach item in model)
            {
                var sach = db.CT_PX.Where(x => x.masach == item.masach && x.PhieuXuat.madl == madl).ToList();
                if (sach != null)
                {
                    List<int> gotmasach = new List<int>();
                    foreach (CT_PX ct in sach)
                    {
                        if (!gotmasach.Contains(item.masach)) 
                        {
                            if (getSLTonDL(item.masach, ct.PhieuXuat.madl) > 0)
                            {
                                var i = new TonkhoDL
                                {
                                    masach = item.masach,
                                    tensach = item.tensach,
                                    giaxuat = item.giaxuat,
                                    soluong = getSLTonDL(item.masach, ct.PhieuXuat.madl)
                                };
                                list.Add(i);
                                gotmasach.Add(item.masach);
                            }
                        }
                       
                    }
               
                }
                
            }
            return list;
        }
        public List<TonSachChuaTra> SachTon_NXB(int manxb)
        {
            var model = db.Saches.ToList();
            List<TonSachChuaTra> list = new List<TonSachChuaTra>();
            foreach (Sach item in model)
            {
                var sach = db.CT_PN.Where(x => x.masach == item.masach && x.PhieuNhap.manxb == manxb).ToList();
                if (sach != null)
                {
                    List<int> gotmasach = new List<int>();
                    foreach (CT_PN ct in sach)
                    {
                        if (!gotmasach.Contains(item.masach))
                        {
                            if (getSLChuaTra(item.masach, ct.PhieuNhap.manxb) > 0)
                            {
                                var i = new TonSachChuaTra
                                {
                                    masach = item.masach,
                                    tensach = item.tensach,
                                    gianhap = item.gianhap,
                                    soluong = getSLChuaTra(item.masach, ct.PhieuNhap.manxb)
                                };
                                list.Add(i);
                                gotmasach.Add(item.masach);
                            }
                        }

                    }

                }

            }
            return list;
        }

        //nhap sach
        public Sach masachdautien(int manxb)
        {
            return db.Saches.Where(x => x.manxb == manxb).OrderByDescending(x => x.masach).FirstOrDefault();
        }
        public int GiaNhap(int masach)
        {
            return db.Saches.Where(x => x.masach == masach).Select(x => x.gianhap).SingleOrDefault();
        }

        //xuat sachs
        public Sach masachdautien()
        {
            return db.Saches.FirstOrDefault();
        }
        public int GiaXuat(int masach)
        {
            return db.Saches.Where(x => x.masach == masach).Select(x => x.giaxuat).SingleOrDefault();
        }

        //phieu thu
        public TonkhoDL masachdautien_DL(int madl)
        {
            var model = db.Saches.ToList();

            TonkhoDL i = new TonkhoDL();
            foreach (Sach item in model)
            {
                var sach = db.CT_PX.Where(x => x.masach == item.masach && x.PhieuXuat.madl == madl).SingleOrDefault();
                if (sach != null)
                {
                    if (getSLTonDL(item.masach, sach.PhieuXuat.madl) > 0)
                    {

                            i.masach = item.masach;
                            i.tensach = item.tensach;
                            i.giaxuat = item.giaxuat;
                            i.soluong = getSLTonDL(item.masach, sach.PhieuXuat.madl);
                        
                      break;
                    }

                }

            }
            return i;
            
        }
       //phieu chi
        public TonSachChuaTra masachdautien_NXB(int manxb)
        {
            var model = db.Saches.ToList();

            TonSachChuaTra i = new TonSachChuaTra();
            foreach (Sach item in model)
            {
                var sach = db.CT_PN.Where(x => x.masach == item.masach && x.PhieuNhap.manxb == manxb).SingleOrDefault();
                if (sach != null)
                {
                    if (getSLChuaTra(item.masach, sach.PhieuNhap.manxb) > 0)
                    {

                        i.masach = item.masach;
                        i.tensach = item.tensach;
                        i.gianhap = item.gianhap;
                        i.soluong = getSLChuaTra(item.masach, sach.PhieuNhap.manxb);
                        break;
                    }

                }

            }
            return i;

        }
    }
}
