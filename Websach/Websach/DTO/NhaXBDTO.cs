﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Websach.Models;


namespace Websach.DTO
{
    public class NhaXBDTO
    {
        QLSachEntities db = null;
        public NhaXBDTO()
        {
            db = new QLSachEntities();
        }

        public List<NXB> List()
        {
            var model = db.NXBs.ToList();
            return model;
        }
        public int Count()
        {
            int count = db.NXBs.Count();
            return count;
        }

        public NXB Details(int id)
        {
            var model = db.NXBs.Find(id);
            return model;
        }
        

    }
}
