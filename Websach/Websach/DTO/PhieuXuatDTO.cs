﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Websach.Models;

namespace Websach.DTO
{
    public class PhieuXuatDTO
    {
        QLSachEntities db = null;
        public PhieuXuatDTO()
        {
            db = new QLSachEntities();
        }
        public List<PhieuXuat> List()
        {
            return db.PhieuXuats.ToList();
        }
        public PhieuXuat Details(int id)
        {
            var model = db.PhieuXuats.Where(x => x.maphieu == id).SingleOrDefault();
            return model;
        }

        public List<CT_PX> ListDetails(int id)
        {
            var model = db.CT_PX.Where(x => x.mapxuat == id).ToList();
            return model;
        }
        public int Count()
        {
            return db.PhieuXuats.ToList().Count(); ;
        }
        public int getNewId()
        {
            var model = db.PhieuXuats.Select(x => x.maphieu).ToList().LastOrDefault();
            int NewId = 0;
            if (model == null)
            {
                NewId = 1;
            }
            else { NewId = model + 1; }
            return NewId;
        }

        private int updateKho(DateTime ngay, int masach, int soluong)
        {
            var modelkho = db.Tonkhoes.Where(x => x.ngayupdatetime <= ngay && x.masach == masach).OrderByDescending(x => x.ngayupdatetime).FirstOrDefault();
            if (modelkho == null)
            {
                return 0;
            }
            else 
            {
                 if (modelkho.soluong - soluong >= 0)
                 {
                    if (modelkho.ngayupdatetime == ngay)
                    {
                        modelkho.soluong -= soluong;
                    }
                    else {
                        Tonkho tonkho = new Tonkho();
                        tonkho.soluong = modelkho.soluong - soluong;
                        tonkho.masach = masach;
                        tonkho.ngayupdatetime = ngay;
                        db.Tonkhoes.Add(tonkho);
                    }
                    //db.SaveChanges();
                    var ngayupdategannhat = db.Tonkhoes.Where(x => x.masach == masach).OrderByDescending(x => x.ngayupdatetime).FirstOrDefault();
                    if (ngayupdategannhat.ngayupdatetime > ngay)
                    {
                        var listupdate = db.Tonkhoes.Where(x => x.masach == masach && x.ngayupdatetime > ngay).OrderBy(x => x.ngayupdatetime).ToList();
                        foreach (var i in listupdate)
                        {
                            if (i.soluong - soluong >= 0)
                            {
                                i.soluong -= soluong;
                            }
                            else { return 0; }
                        }
                        db.SaveChanges();
                    }
                    db.SaveChanges();
                    return 1;
                }
                else { return 0; }
            }
        }
        private int KTKho(DateTime ngay, int masach, int soluong)
        {
            var modelkho = db.Tonkhoes.Where(x => x.ngayupdatetime <= ngay && x.masach == masach).OrderByDescending(x => x.ngayupdatetime).FirstOrDefault();
            if (modelkho == null)
            {
                return 0;
            }
            else
            {
                if (modelkho.soluong - soluong < 0)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
        }
        private void updateCongNo(int madl, int tongno, DateTime ngay)
        {
            var modelcongno = db.CongNo_DL.Where(x => x.ngay <= ngay && x.madl == madl).OrderByDescending(x => x.ngay).FirstOrDefault();
            if (modelcongno == null)
            {
                CongNo_DL congno = new CongNo_DL();
                
                congno.tongno = tongno;
                congno.madl = madl;
                congno.ngay = ngay;
                db.CongNo_DL.Add(congno);
            }
            else if (modelcongno != null && modelcongno.ngay == ngay)
            {
                modelcongno.tongno += tongno;
            }
            else if (modelcongno != null && modelcongno.ngay != ngay)
            {
                CongNo_DL congno = new CongNo_DL();
                congno.tongno = modelcongno.tongno + tongno;
                congno.madl = madl;
                congno.ngay = ngay;
                db.CongNo_DL.Add(congno);
            }    
          //  db.SaveChanges();
            var ngayupdategannhat = db.CongNo_DL.Where(x => x.madl == madl).OrderByDescending(x => x.ngay).FirstOrDefault();
            if (ngayupdategannhat.ngay > ngay)
            {
                var listupdate = db.CongNo_DL.Where(x => x.madl == madl && x.ngay > ngay).OrderBy(x => x.ngay).ToList();
                foreach (var i in listupdate)
                {
                    i.tongno += tongno;
                }
                db.SaveChanges();
            }
            db.SaveChanges();
           
        }
    
        public int Add(PhieuXuat_CT p)
        {
            try
            {
                PhieuXuat pn = new PhieuXuat { maphieu = p.maphieu, madl = p.madl, ngaylap = p.ngaylap, nguoigiao = p.nguoigiao, tongtien = p.tongtien, trangthai = true };

                foreach (var i in p.Chitiet)
                {
                    if (KTKho(pn.ngaylap, i.masach, i.soluong) == 0)
                    {
                        return -1;
                    }
                }
                foreach (var i in p.Chitiet)
                {

                    pn.CT_PX.Add(i);
                    if (updateKho(pn.ngaylap, i.masach, i.soluong) == 0)
                    {
                        return -1;
                    }
                    
                }
                updateCongNo(pn.madl, pn.tongtien, pn.ngaylap);
                db.PhieuXuats.Add(pn);

                var dl = db.Dailies.Find(p.madl);
                if (dl.ngaymuacuoi < p.ngaylap)
                {
                    dl.ngaymuacuoi = p.ngaylap;
                }
                dl.tongno += p.tongtien;
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex) { return 0; }
        }
        

        //Edit
        public CT_PX ItemDetails(int maphieu, int masach)
        {
            var model = db.CT_PX.Where(x => x.mapxuat == maphieu && x.masach == masach).SingleOrDefault();
            return model;
        }
        private int TraSachVaoKho(DateTime ngay, int masach, int soluong)
        {
            var sach = db.Tonkhoes.Where(x => x.ngayupdatetime == ngay && x.masach == masach).SingleOrDefault();
            if (sach == null)
            {
                return 0;
            }
            else
            {
                //lấy sách ra từ kho
                sach.soluong += soluong;
              
                var ngayupdategannhat = db.Tonkhoes.Where(x => x.masach == masach).OrderByDescending(x => x.ngayupdatetime).FirstOrDefault();
                if (ngayupdategannhat.ngayupdatetime > ngay)
                {
                    var listupdate = db.Tonkhoes.Where(x => x.masach == masach && x.ngayupdatetime > ngay).OrderBy(x => x.ngayupdatetime).ToList();
                    foreach (var i in listupdate)
                    {

                        i.soluong += soluong;
                    }
                    db.SaveChanges();

                }
                db.SaveChanges();
                return 1;


            }
        }
        private int TraTienXuat(int madl, int tongno, DateTime ngay)
        {
            var tongCN = db.CongNo_DL.Where(x => x.madl == madl && x.ngay == ngay).SingleOrDefault();
            if (tongCN == null)
            { return 0; }
            else
            {
                tongCN.tongno -= tongno;
                
                var ngayupdategannhat = db.CongNo_DL.Where(x => x.madl == madl).OrderByDescending(x => x.ngay).FirstOrDefault();
                if (ngayupdategannhat.ngay > ngay)
                {
                    var listupdate = db.CongNo_DL.Where(x => x.madl == madl && x.ngay > ngay).OrderBy(x => x.ngay).ToList();
                    foreach (var i in listupdate)
                    {

                        i.tongno -= tongno;

                    }
                    db.SaveChanges();
                }
                db.SaveChanges();
                return 1;

            }
        }
        private void EditItem(CT_PX ct)
        {
            var model = db.CT_PX.Where(x => x.mapxuat == ct.mapxuat && x.masach == ct.masach).SingleOrDefault();

            model.masach = ct.masach;
            model.soluong = ct.soluong;
            model.gia = ct.gia;
            model.thanhtien = ct.thanhtien;
            model.ghichu = ct.ghichu;
            db.SaveChanges();
        }
        public int EditDetail(CT_PX ct)
        {
            try
            {
                //ct trước khi edit
                var model = db.CT_PX.Where(x => x.mapxuat == ct.mapxuat && x.masach == ct.masach).SingleOrDefault();
                var pn = db.PhieuXuats.Where(x => x.maphieu == ct.mapxuat).SingleOrDefault();
                var nxb = db.Dailies.Where(x => x.madl == pn.madl).SingleOrDefault();
               
                if (TraSachVaoKho(model.PhieuXuat.ngaylap, model.masach, model.soluong) == 0)
                {
                    return 0;
                }
                if (updateKho(model.PhieuXuat.ngaylap, ct.masach, ct.soluong) == 0)
                {
                    updateKho(model.PhieuXuat.ngaylap, model.masach, model.soluong);
                    return 0;
                }
                if (TraTienXuat(pn.madl, model.thanhtien, pn.ngaylap) == 0)
                {
                    return 0;
                }
                pn.tongtien -= model.thanhtien;
                nxb.tongno -= model.thanhtien;
                db.SaveChanges();

                //Them lai sach moi sua 
                EditItem(ct);
                pn.tongtien += ct.thanhtien;
                nxb.tongno += ct.thanhtien;
                db.SaveChanges();
                updateCongNo(pn.madl, ct.thanhtien, pn.ngaylap);
                return 1;

            }
            catch (Exception ex) { return 0; }
        }
    }
}