﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Websach.Models;

namespace Websach.DTO
{
    public class NhapSachDTO
    {
        QLSachEntities db = null;

        public NhapSachDTO()
        {
            db = new QLSachEntities();
        }
        public List<PhieuNhap> List()
        {
            return db.PhieuNhaps.ToList();
        }
        public PhieuNhap Details(int id) {
            var model = db.PhieuNhaps.Where(x => x.maphieu == id).SingleOrDefault();
            return model;
        }
        public List<CT_PN> ListDetails(int id)
        {
            var model = db.CT_PN.Where(x => x.mapnhap == id).ToList();
            return model;
        }
        public CT_PN ItemDetails(int maphieu, int masach)
        {
            var model = db.CT_PN.Where(x => x.mapnhap == maphieu && x.masach == masach).SingleOrDefault();
            return model;
        }
        public int Count()
        {
            return db.PhieuNhaps.ToList().Count();
        }
        public int getNewId()
        {
            var model = db.PhieuNhaps.Select(x => x.maphieu).ToList().LastOrDefault();
            int NewId = 0;
            if (model == null)
            {
                NewId = 1;
            }
            else { NewId = model + 1; }
            return NewId;
        }
        private void updateKho(DateTime ngay, int masach, int soluong)
        {
            var soluongtonkho = db.Tonkhoes.Where(x => x.ngayupdatetime <= ngay && x.masach == masach).OrderByDescending(x => x.ngayupdatetime).FirstOrDefault();
            if (soluongtonkho == null)
            {
                
                Tonkho tonkho = new Tonkho();
                tonkho.soluong =  soluong;
                tonkho.masach = masach;
                tonkho.ngayupdatetime = ngay;
                db.Tonkhoes.Add(tonkho);
            }
            else if (soluongtonkho != null && soluongtonkho.ngayupdatetime == ngay)
            {
                soluongtonkho.soluong += soluong;
            }
            else if (soluongtonkho != null && soluongtonkho.ngayupdatetime != ngay) {
                Tonkho tonkho = new Tonkho();
                tonkho.soluong = soluongtonkho.soluong + soluong;
                tonkho.masach = masach;
                tonkho.ngayupdatetime = ngay;
                db.Tonkhoes.Add(tonkho);
            }
          
            var ngayupdategannhat = db.Tonkhoes.Where(x => x.masach == masach).OrderByDescending(x => x.ngayupdatetime).FirstOrDefault();
            if (ngayupdategannhat.ngayupdatetime > ngay)
            {
                var listupdate = db.Tonkhoes.Where(x => x.masach == masach && x.ngayupdatetime > ngay).OrderBy(x => x.ngayupdatetime).ToList(); 
                foreach (var i in listupdate)
                {
                    i.soluong += soluong;
                }
                db.SaveChanges();
               
            }
            db.SaveChanges();
        }
        private void updateCongNo(int manxb,int tongno,DateTime ngay)
        {
            var modelcongno = db.CongNo_NXB.Where(x => x.ngay <= ngay && x.manxb == manxb).OrderByDescending(x => x.ngay).FirstOrDefault();
            if (modelcongno == null)
            {
              
                CongNo_NXB congno = new CongNo_NXB();
                congno.tongno = tongno;
                congno.manxb = manxb;
                congno.ngay = ngay;
                db.CongNo_NXB.Add(congno);
            }
            else if (modelcongno !=null && modelcongno.ngay == ngay)
            {
                modelcongno.tongno += tongno;
            }
            else if (modelcongno !=null && modelcongno.ngay != ngay)
            {
                CongNo_NXB congno = new CongNo_NXB();
                congno.tongno = modelcongno.tongno + tongno;
                congno.manxb = manxb;
                congno.ngay = ngay;
                db.CongNo_NXB.Add(congno);
            }
           
           
            var ngayupdategannhat = db.CongNo_NXB.Where(x => x.manxb == manxb).OrderByDescending(x => x.ngay).FirstOrDefault();
            if (ngayupdategannhat.ngay > ngay)
            {
                var listupdate = db.CongNo_NXB.Where(x => x.manxb == manxb && x.ngay > ngay).OrderBy(x => x.ngay).ToList(); 
                foreach (var i in listupdate)
                {
                    i.tongno += tongno;
                }
                db.SaveChanges();
            }
            db.SaveChanges();
        }
        public int Add(PhieuNhap_CT p)
        {
            try { 
                PhieuNhap pn = new PhieuNhap{maphieu =p.maphieu, manxb = p.manxb, ngaylap = p.ngaylap,nguoigiao = p.nguoigiao,tongtien=p.tongtien,trangthai=true};
                foreach(var i in p.Chitiet)
                {                
                    pn.CT_PN.Add(i);
                    updateKho(pn.ngaylap, i.masach, i.soluong);
                }
                updateCongNo(pn.manxb, pn.tongtien, pn.ngaylap);
                db.PhieuNhaps.Add(pn);
                var nxb = db.NXBs.Find(p.manxb);
                if (nxb.ngaynhapcuoi < p.ngaylap)
                {
                    nxb.ngaynhapcuoi = p.ngaylap;
                }
                nxb.tongno += p.tongtien;
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex) { return 0; }
        }


        //Edit
        private int  TraSachDaNhap(DateTime ngay, int masach, int soluong)
        {
            var sach = db.Tonkhoes.Where(x => x.ngayupdatetime == ngay && x.masach == masach).SingleOrDefault();
            if (sach == null)
            {
                return 0;
            }
            else
            {
                //lấy sách ra từ kho
               
                    sach.soluong -= soluong;
                   
                    var ngayupdategannhat = db.Tonkhoes.Where(x => x.masach == masach).OrderByDescending(x => x.ngayupdatetime).FirstOrDefault();
                    if (ngayupdategannhat.ngayupdatetime > ngay)
                    {
                        var listupdate = db.Tonkhoes.Where(x => x.masach == masach && x.ngayupdatetime > ngay).OrderBy(x => x.ngayupdatetime).ToList();
                        foreach (var i in listupdate)
                        {
                            
                                i.soluong -= soluong;
                        }
                        db.SaveChanges();

                    }
                    db.SaveChanges();
                    return 1;
                
               
            }
        }
        private int TraTienNhap(int manxb, int tongno, DateTime ngay)
        {
            var tongCN = db.CongNo_NXB.Where(x => x.manxb == manxb && x.ngay == ngay).SingleOrDefault();
            if (tongCN == null)
            { return 0; }
            else
            {
                tongCN.tongno -= tongno;
               
                var ngayupdategannhat = db.CongNo_NXB.Where(x => x.manxb == manxb).OrderByDescending(x => x.ngay).FirstOrDefault();
                if (ngayupdategannhat.ngay > ngay)
                {
                    var listupdate = db.CongNo_NXB.Where(x => x.manxb == manxb && x.ngay > ngay).OrderBy(x => x.ngay).ToList();
                    foreach (var i in listupdate)
                    {
                       
                         i.tongno -= tongno; 
                        
                    }
                    db.SaveChanges();
                }
                db.SaveChanges();
                return 1;
           
            }
        }
        private void EditItem(CT_PN ct)
        {
            var model = db.CT_PN.Where(x => x.mapnhap == ct.mapnhap && x.masach == ct.masach).SingleOrDefault();
            
            model.masach = ct.masach;
            model.soluong = ct.soluong;
            model.thanhtien = ct.thanhtien;
            model.gianhap = ct.gianhap;
            model.ghichu = ct.ghichu;
            db.SaveChanges();
        }
        public int EditDetail(CT_PN ct)
        {
            try
            {
                //ct trước khi edit
                var model = db.CT_PN.Where(x => x.mapnhap == ct.mapnhap && x.masach == ct.masach).SingleOrDefault();
                var pn = db.PhieuNhaps.Where(x => x.maphieu == ct.mapnhap).SingleOrDefault();
                var nxb = db.NXBs.Where(x => x.manxb == pn.manxb).SingleOrDefault();
                if(TraSachDaNhap(model.PhieuNhap.ngaylap, model.masach, model.soluong) == 0)
                {
                    return 0;
                }
                if (TraTienNhap(pn.manxb, model.thanhtien, pn.ngaylap) == 0)
                {
                    return 0;
                }
                pn.tongtien -= model.thanhtien;
                nxb.tongno -= model.thanhtien;
                db.SaveChanges();

                //Them lai sach moi sua 
                EditItem(ct);
                pn.tongtien += ct.thanhtien;
                nxb.tongno += ct.thanhtien;
                db.SaveChanges();
                updateKho(model.PhieuNhap.ngaylap, ct.masach, ct.soluong);
                updateCongNo(pn.manxb, ct.thanhtien, pn.ngaylap);
                return 1;
                
            }
            catch (Exception ex) { return 0; }
        }
    }
}
