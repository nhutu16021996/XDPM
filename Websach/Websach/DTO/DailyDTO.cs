﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Websach.DTO;
using Websach.Models;

namespace Websach.DTO
{
    public class DailyDTO
    {
        QLSachEntities db = null;
        public DailyDTO()
        {
            db = new QLSachEntities();
        }

        public List<Daily> List()
        {
            var model = db.Dailies.Where(x => x.trangthai == true).ToList();
            return model;
        }
        public int Count()
        {
            int count = db.Dailies.Where(x => x.trangthai == true).Count();
            return count;
        }
        public Daily tongno_gioihan(int madl)
        {
            return db.Dailies.Where(x=>x.madl == madl).SingleOrDefault();
        }
        public string Gettendl(int madl)
        {
            return db.Dailies.Where(x => x.madl == madl).Select(x => x.tendl).SingleOrDefault();
        }
        public int Insert(Daily dl)
        {
            try{
                dl.trangthai = true;
                db.Dailies.Add(dl);
                db.SaveChanges();
                return dl.madl;
            }catch(Exception ex){
                return 0;
            }
           
        }
        public int Edit(Daily dl)
        {
            try {
                var model = db.Dailies.Find(dl.madl);
                if (model == null)
                {
                    return 0;

                }
                else
                {
                    model.tendl = dl.tendl;
                    model.notoida = dl.notoida;
                    model.sdt = dl.sdt;
                    model.diachi = dl.diachi;
                    model.email = dl.email;
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex) { return 0; }
        }
        public int Delete(int id)
        {
            try {
                var model = db.Dailies.Find(id);
                if (model == null)
                {
                    return 0;
                }
                else {
                    model.trangthai = false;
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex) { return 0; }
        }
        public Daily Details(int id)
        {
                var model = db.Dailies.Find(id);
                return model;
        }

         
    }
}
