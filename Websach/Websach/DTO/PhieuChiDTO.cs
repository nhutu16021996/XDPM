﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Websach.Models;

namespace Websach.DTO
{
    public class PhieuChiDTO
    {
        QLSachEntities db = null;
        public PhieuChiDTO()
        {
            db = new QLSachEntities();
        }
        public List<PhieuChi> List()
        {
            return db.PhieuChis.ToList();
        }
        public List<PhieuChi> List(DateTime start,DateTime end)
        {
            return db.PhieuChis.Where(x=>x.ngaylap>=start && x.ngaylap<=end).ToList();
        }
        public List<PhieuChi> ListPC_NXB(int id)
        {
            return db.PhieuChis.Where(x => x.manxb == id).ToList();
        }

        public PhieuChi Details(int id)
        {
            return db.PhieuChis.Where(x => x.maphieu == id).SingleOrDefault();
        }
        public List<CT_PC> ListDetails(int id)
        {
            var model = db.CT_PC.Where(x => x.mapchi == id).ToList();
            return model;
        }

        public int Tongchi(DateTime start, DateTime end)
        {
            int tongchi = db.PhieuChis.Where(x => x.ngaylap >= start && x.ngaylap <= end).Sum(x => x.tongchi);
            return tongchi;
        }
        public int getNewId()
        {
            var model = db.PhieuChis.Select(x => x.maphieu).ToList().LastOrDefault();
            int NewId = 0;
            if (model == null)
            {
                NewId = 1;
            }
            else { NewId = model + 1; }
            return NewId;
        }
        public string getNameNXB(int id)
        {
            var model = db.NXBs.Where(x => x.manxb == id).SingleOrDefault();
            return model.tennxb;
        }
        public int getTongNo(int id)
        {
            var model = db.NXBs.Where(x => x.manxb == id).SingleOrDefault();
            return model.tongno;
        }

        private int updateCongNo(int manxb, int tongno, DateTime ngay)
        {
            var modelcongno = db.CongNo_NXB.Where(x => x.ngay <= ngay && x.manxb == manxb).OrderByDescending(x => x.ngay).FirstOrDefault();
            if (modelcongno == null || modelcongno.tongno - tongno < 0)
            {
                return 0;
            }
            else
            {
                if (modelcongno.ngay == ngay)
                {
                    modelcongno.tongno -= tongno;
                }
                else
                {
                    CongNo_NXB congno = new CongNo_NXB();
                    congno.manxb = manxb;
                    congno.ngay = ngay;
                    congno.tongno = modelcongno.tongno - tongno;
                    db.CongNo_NXB.Add(congno);
                }

                db.SaveChanges();
                var ngayupdategannhat = db.CongNo_NXB.Where(x => x.manxb == manxb).OrderByDescending(x => x.ngay).FirstOrDefault();
                if (ngayupdategannhat.ngay > ngay)
                {
                    var listupdate = db.CongNo_NXB.Where(x => x.manxb == manxb && x.ngay > ngay).OrderBy(x => x.ngay).ToList();
                    foreach (var i in listupdate)
                    {
                        if (i.tongno - tongno < 0)
                        {
                            return 0;
                        }
                        else
                        {
                            i.tongno -= tongno;
                           
                        }
                    }
                    db.SaveChanges();
                    return 1;
                }
                return 1;
            }

        }
        public int getSLChuaTra(int masach, int manxb,DateTime ngay)
        {
            int tongSLnhap = db.CT_PN.Where(x => x.PhieuNhap.manxb == manxb && x.masach == masach && x.PhieuNhap.ngaylap <= ngay).Select(x => x.soluong).DefaultIfEmpty().Sum();
            int tongSLchi = db.CT_PC.Where(x => x.PhieuChi.manxb == manxb && x.masach == masach && x.PhieuChi.ngaylap <= ngay).Select(x => x.soluong).DefaultIfEmpty().Sum();
            int tongTon = tongSLnhap - tongSLchi;
          
             return tongTon;
        }
        public int Add(PhieuChi_CT p)
        {
            try
            {
                PhieuChi pn = new PhieuChi { maphieu = p.maphieu, manxb = p.manxb, ngaylap = p.ngaylap, tongchi = p.tongchi, trangthai = true };
             
                foreach (var i in p.Chitiet)
                {
                    // var tontrcdo = db.Tonkho_DL.Where(x => x.masach == i.masach && x.madl == pn.madl).Select(x => x.soluong).SingleOrDefault();
                    
                    if (getSLChuaTra(i.masach, pn.manxb, pn.ngaylap) - i.soluong < 0)
                    {
                        return -1;
                    }
                    else
                    {
                        pn.CT_PC.Add(i);
                    }

                }
                if (updateCongNo(pn.manxb, pn.tongchi, pn.ngaylap) == 0)
                {
                    return 0;
                }

                db.PhieuChis.Add(pn);
                var dl = db.NXBs.Find(p.manxb);
                dl.tongno -= p.tongchi;
                db.SaveChanges();
                return 1;    
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

    }
}