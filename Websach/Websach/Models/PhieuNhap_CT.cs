﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Websach.Models
{
    public class PhieuNhap_CT
    {
        public int maphieu { get; set; }
        public DateTime ngaylap { get; set; }
        public int manxb { get; set; }
        public string nguoigiao { get; set; }
        public int tongtien { get; set; }
        public List<CT_PN> Chitiet { get; set; }
    }
}