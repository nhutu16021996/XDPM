﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Websach.Models
{
    public class PhieuThu_CT
    {
        public int maphieu { get; set; }
        public int madl { get; set; }
        public int tongthu { get; set; }
        public int conno { get; set; }
        public System.DateTime ngaylap { get; set; }
        public List<CT_PT> Chitiet { get; set; }
    }
}