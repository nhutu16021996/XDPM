﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Websach.Models
{
    public class CongNoNXB
    {
        public int manxb { get; set; }
        public System.DateTime ngay { get; set; }
        public int tongno { get; set; }
        public string tennxb { get; set; }
    }
}