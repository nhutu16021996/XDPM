﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Websach.Models
{
    public class PhieuChi_CT
    {
        public int maphieu { get; set; }
        public DateTime ngaylap { get; set; }
        public int manxb { get; set; }      
        public int tongchi { get; set; }
        public List<CT_PC> Chitiet { get; set; }
    }
}