﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Websach.Models
{
    public class TonSachChuaTra
    {
        public int manxb { get; set; }
        public int masach { get; set; }
        public int soluong { get; set; }
        public string tensach { get; set; }
        public int gianhap { get; set; }
    }
}