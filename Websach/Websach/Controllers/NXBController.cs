﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websach.DTO;


namespace Websach.Controllers
{
    public class NXBController : BaseController
    {
        //
        // GET: /NXB/

        public ActionResult Index()
        {
            var dao = new NhaXBDTO();
            
            return View(dao.List());
        }
        public ActionResult Detail(int id)
        {
            var dao = new NhaXBDTO();
            var pc = new PhieuChiDTO();
            var nxb = dao.Details(id);
            var listsach = new SachDTO();
            ViewBag.DsSachCanTL = listsach.SachTon_NXB(id);
           
            if (nxb != null)
            {
                return View(nxb);
            }
            else
            {
                SetAlert("Không tìm được nhà xuất bản", "error");
                return RedirectToAction("Index", "NXB");
            }
        }
      
    }
}
