﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Websach.DTO;
using Websach.Models;

namespace Websach.Controllers
{
    public class XuatsachController : BaseController
    {
        //
        // GET: /Xuatsach/

        public ActionResult Index()
        {
            var dao = new PhieuXuatDTO();
            return View(dao.List());
        }
        public ActionResult Detail(int id)
        {
            var dao = new PhieuXuatDTO();
            var px = dao.Details(id);

            if (px == null)
            {
                SetAlert("Không tìm được phiếu xuất", "error");
                return RedirectToAction("Index", "Xuatsach");
            }
            else
            {
                ViewBag.DS_CTX = dao.ListDetails(id);
                return View(px);
            }
        }
        public ActionResult tongno(int id)
        {
            var dao = new DailyDTO();
            // If one exists, return it
            var tongno = dao.tongno_gioihan(id);
            if (tongno != null)
            {
                var result = new
                {
                    tongno = tongno.tongno,
                    notoida = tongno.notoida,
                  
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            // Otherwise, something is wrong
            return HttpNotFound(); 
         
        }
        [HttpGet]
        public ActionResult Create()
        {
             var getid = new PhieuXuatDTO();
             var dl = new DailyDTO();
             ViewBag.Maphieuxuat = getid.getNewId();
             ViewBag.LayDSDL = new SelectList(dl.List(), "madl", "tendl", null);
            return View();
        }

        [HttpPost]
        public JsonResult Create(PhieuXuat_CT p)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                var dao = new PhieuXuatDTO();
                int bo = dao.Add(p);
                if (bo== 1)
                {

                    status = true;
                    SetAlert("Thêm phiếu thành công", "success");
                }
                else if (bo == 0)
                {
                    status = false;
                    SetAlert("Thêm phiếu không thành công", "error");
                }
                else
                {
                    status = false;
                    SetAlert("Không đủ số lượng để xuất", "error");
                }

            }
            else
            {
                status = false;
                SetAlert("Nhập thông tin không hợp lệ", "error");
            }
            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult CreateDetail()
        {
            var sach = new SachDTO();
            ViewBag.LayDSsach = new SelectList(sach.List(), "masach", "tensach", null);
            var masach = sach.masachdautien();
            ViewBag.GiaSachDauTien = sach.GiaXuat(masach.masach);
            return View();
        }

        public ActionResult giaxuat(int id)
        {
            var dao = new SachDTO();
            var gia = dao.GiaXuat(id);
            return Json(gia, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditDetail(int maphieu, int masach)
        {
            var dao = new PhieuXuatDTO();
            //var dsSach = new SachDTO();
            //ViewBag.DSsach = new SelectList(dsSach.List(), "masach", "tensach", masach);
            return View(dao.ItemDetails(maphieu, masach));
        }

        [HttpPost]
        public ActionResult EditDetail(CT_PX ct)
        {
            int id = ct.mapxuat;
            try
            {
                if (ModelState.IsValid)
                {
                    var dao = new PhieuXuatDTO();
                    int kq = dao.EditDetail(ct);

                    if (kq == 1)
                    {
                        SetAlert("Sửa phiếu thành công", "success");
                        return RedirectToAction("Detail", "Xuatsach", new { id = id });

                    }
                    else
                    {
                        SetAlert("Sửa phiếu không thành công", "error");
                        return RedirectToAction("Detail", "Xuatsach", new { id = id });
                    }
                }
                SetAlert("Dữ liệu sửa không hợp lệ", "error");
                return RedirectToAction("Detail", "Xuatsach", new { id = id });
            }
            catch
            {
                return RedirectToAction("Detail", "Xuatsach", new { id = id });
            }
        }

    }
}
