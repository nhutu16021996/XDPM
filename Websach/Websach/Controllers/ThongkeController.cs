﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websach.DTO;

namespace Websach.Controllers
{
    public class ThongkeController : BaseController
    {
        //
        // GET: /Thongke/

        [HttpGet]
        public ActionResult Tonkho()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Tonkho(DateTime ngay)
        {
           
                var dao = new ThongkeDTO();
                var data =dao.Tonkho(ngay);
                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            
        }

        [HttpGet]
        public ActionResult CongnoDL()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CongnoDL(DateTime ngay)
        {

            var dao = new ThongkeDTO();
            var data = dao.CongNoDL(ngay);
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult CongnoNXB()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CongnoNXB(DateTime ngay)
        {

            var dao = new ThongkeDTO();
            var data = dao.CongNoNXB(ngay);
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]       
        public ActionResult Doanhthu()
        {
            return View();
        }

        public ActionResult LoadThu_Chi(string start, string end)
        {
            DateTime sd = DateTime.ParseExact(start, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            DateTime ed = DateTime.ParseExact(end, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            var thu = new PhieuThuDTO();
            var chi = new PhieuChiDTO();
            ViewBag.DsThu = thu.List(sd,ed);
            ViewBag.DsChi = chi.List(sd,ed);
            return View();
        }
    }
}
