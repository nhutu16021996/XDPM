﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websach.DTO;
using Websach.Models;

namespace Websach.Controllers
{
    public class PhieuchiController : BaseController
    {
        //
        // GET: /Phieuchi/

        public ActionResult Index(int id)
        {
            var dao = new PhieuChiDTO();
            ViewBag.Maphieuchi = dao.getNewId();
            ViewBag.TenNXB = dao.getNameNXB(id);
            ViewBag.manxb = id;
            ViewBag.SoNo = dao.getTongNo(id);
            return View(dao.ListPC_NXB(id));
        }
        public ActionResult Detail(int id)
        {
            var dao = new PhieuChiDTO();
            var pt = dao.Details(id);
            if (pt == null)
            {
                SetAlert("Không tìm được phiếu chi", "error");
                return RedirectToAction("Index", "Phieuthu",id);
            }
            else
            {
                ViewBag.DS_CTC = dao.ListDetails(id);
                return View(pt);
            }
        }

        [HttpGet]
        public ActionResult CreateDetails(int id)
        {
            var dao = new SachDTO();
            ViewBag.LayDSsach = new SelectList(dao.SachTon_NXB(id), "masach", "tensach", null);
            var masach = dao.masachdautien_NXB(id);
            ViewBag.GiaSachDauTien = masach.gianhap;
            return View();
        }

        [HttpPost]
        public JsonResult Create(PhieuChi_CT pc)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                var dao = new PhieuChiDTO();
                int kq= dao.Add(pc);
                if (kq == 1)
                {

                    status = true;
                    SetAlert("Thêm phiếu thành công", "success");
                }
                else if (kq == -1)
                {
                    status = false;
                    SetAlert("Số lượng chi lớn hơn số lượng sách nhập", "error");
                }

                else
                {
                    status = false;
                    SetAlert("Tổng chi vượt hơn tổng nợ ", "error");
                }

            }
            else
            {
                status = false;
                SetAlert("Nhập thông tin phiếu chưa đúng", "error");
            }
            return new JsonResult { Data = new { status = status } };
        }
        public ActionResult gianhap(int id)
        {
            var dao = new SachDTO();
            var gia = dao.GiaNhap(id);
            return Json(gia, JsonRequestBehavior.AllowGet);
        }
       
    }
}
