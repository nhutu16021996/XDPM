﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websach.DTO;
using Websach.Models;

namespace Websach.Controllers
{
    public class NhapsachController : BaseController
    {
        //
        // GET: /Nhapsach/

        public ActionResult Index()
        {
            var dao = new NhapSachDTO();
            return View(dao.List());
        }
        public ActionResult Detail(int id)
        {
            var dao = new NhapSachDTO();
            var pn = dao.Details(id);         
            if (pn == null)
            {
                SetAlert("Không tìm được phiếu nhập", "error");
                return RedirectToAction("Index", "Nhapsach");
            }
            else {
                ViewBag.DS_CTN = dao.ListDetails(id);
                return View(pn);
            }
            
        }
        [HttpGet]
        public ActionResult Create()
        {
            var dao = new NhaXBDTO();
            var getId = new NhapSachDTO();
            ViewBag.LayDSNXB = new SelectList(dao.List(), "manxb", "tennxb", null);
            ViewBag.Maphieunhap = getId.getNewId();
            return View();
        }
        [HttpPost]
        public JsonResult Create(PhieuNhap_CT p)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                var dao = new NhapSachDTO();
                if (dao.Add(p) == 1)
                {

                    status = true;
                    SetAlert("Thêm phiếu thành công", "success");
                }
                else
                {
                    status = false;
                    SetAlert("Thêm phiếu không thành công", "error");
                }

            }
            else
            {
                status = false;
                SetAlert("Thêm phiếu không thành công", "error");
            }
            return new JsonResult { Data = new { status = status } };
        }
        [HttpGet]
        public ActionResult CreateDetail(int id)
        {
            var dao = new SachDTO();
            ViewBag.LayDSsach = new SelectList(dao.Sach_NXB(id), "masach", "tensach", null);
            var masach = dao.masachdautien(id);
            ViewBag.GiaSachDauTien = dao.GiaNhap(masach.masach);
            return View();
        }
        public ActionResult gianhap(int id)
        {
            var dao = new SachDTO();
            var gia = dao.GiaNhap(id);
            return Json(gia, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            var dao = new NhapSachDTO();
            var dsNXB = new NhaXBDTO();
            var pn = dao.Details(id);
            ViewBag.LayDSNXB_Edit = new SelectList(dsNXB.List(), "manxb", "tennxb", pn.manxb);
            if (pn == null)
            {
                SetAlert("Không tìm được phiếu nhập", "error");
                return RedirectToAction("Index", "Nhapsach");
            }
            else
            {
                ViewBag.DS_CTN = dao.ListDetails(id);
                return View(pn);
            }
        }

        [HttpGet]
        public ActionResult EditDetail(int maphieu, int masach)
        {
            var dao = new NhapSachDTO();
            //var dsSach = new SachDTO();
            //ViewBag.DSsach = new SelectList(dsSach.List(), "masach", "tensach", masach);
            return View(dao.ItemDetails(maphieu, masach));
        }

        [HttpPost]
        public ActionResult EditDetail(CT_PN ct)
        {
            int id = ct.mapnhap;
            try
            {
                if (ModelState.IsValid)
                {
                    var dao = new NhapSachDTO();
                    int kq = dao.EditDetail(ct);
                   
                    if (kq == 1)
                    {
                        SetAlert("Sửa phiếu thành công", "success");
                        return RedirectToAction("Detail", "Nhapsach", new { id = id });
                      
                    }
                    else
                    {
                        SetAlert("Sửa phiếu không thành công", "error");
                        return RedirectToAction("Detail", "Nhapsach", new { id = id });
                    }
                }
                SetAlert("Dữ liệu sửa không hợp lệ", "error");
                return RedirectToAction("Detail", "Nhapsach", new { id = id });
            }
            catch
            {
                return RedirectToAction("Detail", "Nhapsach", new { id = id });
            }
        }

    }
}
