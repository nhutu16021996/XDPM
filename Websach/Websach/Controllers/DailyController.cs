﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using Websach.DTO;
using Websach.Models;



namespace Websach.Controllers
{

    public class DailyController : BaseController
    {
        //

        public ActionResult Index()
        {
            var dao = new DailyDTO();
            return View(dao.List());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Daily dl)
        {
            try{
                if (ModelState.IsValid)
                {
                    var dao = new DailyDTO();
                    int id = dao.Insert(dl);
                    if (id > 0)
                    {
                        SetAlert("Thêm đại lý thành công", "success");
                        return RedirectToAction("Index", "Daily");
                    }
                    else
                    {
                        SetAlert("Thêm đại lý không thành công","error");
                    }
                }
                SetAlert("Dữ liệu nhập không hợp lệ", "error");
                return RedirectToAction("Index", "Daily");
            }
            catch 
            {
                return View();
            }
          
        }

        public ActionResult Detail(int id)
        {
            var dao = new DailyDTO();
            var listsach= new SachDTO();
            var dl = dao.Details(id);
            ViewBag.DSsach_DL = listsach.SachTon_DL(id);
            if(dl != null)
            {
                return View(dl);
            }
            else
            {
                SetAlert("Không tìm được đại lý", "error");
                return RedirectToAction("Index", "Daily");
            }

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var dao = new DailyDTO();
            var ctdl = dao.Details(id);
            return View(ctdl);

        }
        [HttpPost]
        public ActionResult Edit(Daily dl)
        {
           
            try
            {
                if (ModelState.IsValid)
                {
                    var dao = new DailyDTO();
                    int id = dao.Edit(dl);
                    if (id > 0)
                    {
                        SetAlert("Sửa đại lý thành công", "success");
                        return RedirectToAction("Index", "Daily");
                    }
                    else
                    {
                        SetAlert("Sửa đại lý không thành công", "error");
                    }
                }
                SetAlert("Dữ liệu sửa không hợp lệ", "error");
                return RedirectToAction("Index", "Daily");
            }
            catch
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var dao = new DailyDTO();
            var ctdl = dao.Details(id);
            return View(ctdl);
        }
        [HttpPost]
        public ActionResult Delete(Daily dl)
        {
            var dao = new DailyDTO();
            if (dao.Delete(dl.madl) == 1)
            {
                SetAlert("Xóa đại lý thành công", "success");
               
            }
            else
            {
                SetAlert("Xóa đại lý không thành công", "error");
               
            }
            return RedirectToAction("Index", "Daily");
        }

        public ActionResult Export(int id)
        {
            try
            {
                var listsach = new SachDTO();
                var list = listsach.SachTon_DL(id);
                var dl = new DailyDTO();
                string tendl = dl.Gettendl(id);
                string path= "~/Content/"+tendl+"-"+DateTime.Now.Month+".xlsx";
                string filepath = Server.MapPath(path);
                FileInfo Files = new FileInfo(filepath);
                ExcelPackage excel = new ExcelPackage(Files);
               var sheet = excel.Workbook.Worksheets.Add("Báo cáo số lượng sách bán được");

               sheet.Cells["A1:E1"].Merge = true;
               sheet.Cells["A1:E1"].Value = "Báo cáo số lượng sách bán Tháng " + DateTime.Now.Month;
               sheet.Cells["A1:E1"].Style.Font.Bold = true;
               sheet.Cells[2, 1].Value = "Tên sách";
               sheet.Cells[2, 2].Value = "Số lượng chưa thanh toán";
               sheet.Cells[2, 3].Value = "Số lượng bán được";
               sheet.Cells[2, 4].Value = "Đơn giá";
               sheet.Cells[2, 5].Value = "Thành tiền";
               int i = 3;
                foreach(TonkhoDL ct in list)
                {
                    sheet.Cells[i, 1].Value = ct.tensach;
                    sheet.Cells[i, 2].Value = ct.soluong;
                    sheet.Cells[i, 3].Value = "";
                    sheet.Cells[i, 4].Value = ct.giaxuat;
                    sheet.Cells[i, 5].Value = ct.soluong * ct.giaxuat;
                    i++;
                }
                excel.Save();
                SetAlert("Xuất file thành công", "success");
                ViewBag.id = id;
                return View();
               
            }
            catch (Exception ex)
            {
                SetAlert("Lỗi không thể xuất file", "error");
                return View();
            }
            
        }

    }
}
