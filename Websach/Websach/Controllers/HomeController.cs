﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Websach.DTO;

namespace PHsach.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var dl = new DailyDTO();
            var nxb = new NhaXBDTO();
            var pnhap = new NhapSachDTO();
            var pxuat=  new PhieuXuatDTO();
            ViewBag.TongDL = dl.Count();
            ViewBag.TongNXB = nxb.Count();
            ViewBag.TongNhap = pnhap.Count();
            ViewBag.TongXuat = pxuat.Count();
            return View();
        }
    }
}