﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websach.DTO;
using Websach.Models;

namespace Websach.Controllers
{
    public class PhieuthuController : BaseController
    {
        //
        // GET: /Phieuchi/

        public ActionResult Index(int id)
        {
            var dao = new PhieuThuDTO();
            ViewBag.Maphieuthu = dao.getNewId();
            ViewBag.TenDl_thu = dao.getNameDL(id);
            ViewBag.madl = id;
            ViewBag.SoNo = dao.getTongNo(id);
            return View(dao.ListPT_DL(id));
        }

        public ActionResult Detail(int id)
        {
            var dao = new PhieuThuDTO();
            var pt = dao.Details(id);

            if (pt == null)
            {
                SetAlert("Không tìm được phiếu thu", "error");
                return RedirectToAction("Index", "Phieuthu",id);
            }
            else
            {
                ViewBag.DS_CTT = dao.ListDetails(id);
                return View(pt);
            }
        }
        [HttpGet]
        public ActionResult CreateDetails(int id)
        {
            var dao = new SachDTO();
            ViewBag.LayDSsach = new SelectList(dao.SachTon_DL(id), "masach", "tensach", null);
            var masach = dao.masachdautien_DL(id);
            ViewBag.GiaSachDauTien = masach.giaxuat;
            return View();
        }

        [HttpPost]
        public JsonResult Create(PhieuThu_CT p)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                var dao = new PhieuThuDTO();
                var kq =dao.Add(p);
                if (kq == 1)
                {

                    status = true;
                    SetAlert("Thêm phiếu thành công", "success");
                }
                else if(kq == -1)
                {
                    status = false;
                    SetAlert("Số lượng sách báo cáo lớn hơn số lượng sách mà đại lý đã đặt", "error");
                }
                else
                {
                    status = false;
                    SetAlert("Tổng thu vượt hơn tổng nợ của đại lý", "error");
                }

            }
            else
            {
                status = false;
                SetAlert("Nhập thông tin phiếu chưa đúng", "error");

            }
            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult giaxuat(int id)
        {
            var dao = new SachDTO();
            var gia = dao.GiaXuat(id);
            return Json(gia, JsonRequestBehavior.AllowGet);
        }
    }
}
