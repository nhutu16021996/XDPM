--drop database qlsach
CREATE DATABASE QLSach

--Sach
--drop table sach
CREATE TABLE Sach(
   masach int IDENTITY(1,1) primary key,
   manxb int, 
   tensach NVARCHAR (100) NOT NULL,
   tacgia NVARCHAR (50),
   hinh NVARCHAR (255),
   mota nvarchar(700),
   gianhap int not null,
   giaxuat int not null,
   trangthai bit,
)


insert into Sach (manxb, tensach, tacgia, mota,gianhap,giaxuat, trangthai) values (1, N'Chuyến Thư Miền Nam','Antoine De Saint-Exupéry',N'Cùng lúc là tự truyện của người phi công và thước phim tài liệu trữ tình chứa đựng những suy tư về chủ nghĩa anh hùng',10000,12000,1)
insert into Sach (manxb, tensach, tacgia, mota,gianhap,giaxuat, trangthai) values (5, N'Những Chuyến Phiêu Lưu Mới Của Hoàng Tử Bé - Giải Cứu Hành Tinh Thời Gian', 'Antoine De Saint-Exupéry ', N' Hành tinh Thời gian đang rơi vào cảnh im lặng đáng sợ đã có ngày bừng tỉnh lại, tươi vui như chưa hề có bất kỳ tai họa nào xảy ra. Và rồi, sự sống lại đến nơi Hành tinh Chim lửa đang u ám xám xịt, Hành tinh Gió đang lạnh lẽo hoang vu…',10000,12000,0)
insert into Sach (manxb, tensach, tacgia, mota,gianhap,giaxuat, trangthai) values (1, N'Cô Gái Đến Từ Hôm Qua', N'Nguyễn Nhật Ánh', N' Và điều nghịch lý ấy xem ra càng “trớ trêu’ hơn, khi như một định mệnh, Thư nhận ra Việt An, cô bạn học thông minh thường làm mình bối rối bấy lâu nay chính là Tiểu Li, con bé hàng xóm ngốc nghếch từng hứng chịu những trò nghịch ngợm của mình hồi xưa.',10000,12000,0)
insert into Sach (manxb, tensach, tacgia, mota,gianhap,giaxuat, trangthai) values (3, N'Xuôi Nhớ Ngược Thương', N'Antoine De Saint-Exupéry ', N'Xuôi nhớ ngược thương là một cuốn sách buồn! Mẫu số chung của cả cuốn sách là một nỗi niềm không vui của những kẻ đã yêu! Mọi thứ tình cảm trong những trang tản văn này đều vì lý do yêu người không yêu ta mà lở dỡ.',10000,12000,1)
insert into Sach (manxb, tensach, tacgia, mota,gianhap,giaxuat, trangthai) values (2, N'Bóng Ma Ký Ức','Gillian Flynn',N'Libby Day chỉ mới 7 tuổi khi mẹ và hai chị gái bị sát hại đẫm máu trong “Cuộc hiến tế của quỷ Sa-tăng” tại Kinnakee, Kansas. Cô đã may mắn sống sót và trở nên nổi tiếng vì đã làm chứng chống lại người anh trai 15 tuổi, khẳng định Ben Day là kẻ xuống tay giết người.',10000,12000,0)
INSERT INTO Sach (manxb, tensach, tacgia, hinh,gianhap,giaxuat, mota, trangthai)
VALUES (1,N'Bạch dạ hành','Higashino Keigo','',N'Sách dành cho tuổi mới lớn.',10000,12000,'');
INSERT INTO Sach ( manxb, tensach, tacgia, hinh, mota,gianhap,giaxuat, trangthai)
VALUES (2,N'Kiếp sau','Marc Levy','',N'Sách dành cho tuổi mới lớn.',10000,12000,'');
INSERT INTO Sach ( manxb, tensach, tacgia, hinh, mota,gianhap,giaxuat, trangthai)
VALUES (3,N'Thám tử gà mờ',N'Lượng Lượng','',N'Sách dành cho tuổi mới lớn.',10000,12000,'');
INSERT INTO Sach (manxb, tensach, tacgia, hinh, mota,gianhap,giaxuat, trangthai)
VALUES (2,N'Nhân nào quả ấy',N'Vương Trí Nhàn','',N'Sách văn học Việt Nam.',10000,12000,'');
INSERT INTO Sach (manxb, tensach, tacgia, hinh, mota,gianhap,giaxuat, trangthai)
VALUES (2,N'Đọc tôi bên bến lạ',N'Đoàn Cẩm Thi','',N'Sách văn học Việt Nam.',10000,12000,'');
INSERT INTO Sach ( manxb, tensach, tacgia, hinh, mota,gianhap,giaxuat, trangthai)
VALUES (2,N'Bức xúc không làm ta vô can',N'Đặng Hoàng Giang','',N'Sách văn học Việt Nam.',10000,12000,'');
INSERT INTO Sach ( manxb, tensach, tacgia, hinh, mota,gianhap,giaxuat, trangthai)
VALUES (4,N'Nỗi lo âu về địa vị','Alain de Botton','',N'Sách triết học.','');
INSERT INTO Sach ( manxb, tensach, tacgia, hinh, mota,gianhap,giaxuat, trangthai)
VALUES (4,N'Sự an ủi của triết học','Alain de Botton','',N'Sách triết học.',10000,12000,'');
INSERT INTO Sach ( manxb, tensach, tacgia, hinh, mota,gianhap,giaxuat, trangthai)
VALUES (4,N'Luận về yêu','Alain de Botton','',N'Sách triết học.',10000,12000,'');
INSERT INTO Sach ( manxb, tensach, tacgia, hinh, mota,gianhap,giaxuat, trangthai)
VALUES (5,N'Chuyến phiêu lưu diệu kỳ của Edward Tulane','Kate DiCamillo','',N'Sách thiếu nhi.',10000,12000,'');
--Dai ly

--DROP TABLE Daily;
CREATE TABLE Daily(
   madl int IDENTITY(1,1) primary key,
   tendl NVARCHAR (50) NOT NULL,
   diachi NVARCHAR (250),
   email NVARCHAR (255),
   sdt nvarchar(15),
   tongno int not null,
   notoida int not null,
   ngaymuacuoi datetime,
   trangthai bit,
);

insert into Daily (tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai) values (N'Phương Nam', N'123 Nguyễn Xí Q1 TP. Hồ Chí Minh', 'phuongnam@gmail.com', '0123456786', '', '', '', 0)
insert into Daily (tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai) values (N'Hồng Hà', N'234 Ngô Thì Nhậm Q3 TP Hồ Chí Minh', 'hongha@gmail.com', '05658876587', '', '', '', 0)
insert into Daily (tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai) values (N'Nguyễn Văn Cừ', N'345 An Bình Q2 TP Hồ Chí Minh', 'nguyenvancu@outlook.com.vn', '05794857773', '', '', '', 1)
insert into Daily (tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai) values (N'Minh Khai', N'456 Trần Đình Xu Quận Ba Đình, Hà Nội', 'minhkhai@yahoo.com.vn', '0737639876', '', '', '', 1)
insert into Daily (tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai) values (N'Nguyễn Tri Phương', N'273 An Dương Vương Q5 TP Hồ Chí Minh', 'nguyentriphuong@gmail.com', '0487523786', '', '', '', 1)
INSERT INTO Daily ( tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai)
VALUES ('Văn Hóa Việt Long', '14/35, Đào Duy Anh, P.9, Q. Phú Nhuận', '', '02838452708', 0, 10000000, '', '');
INSERT INTO Daily ( tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai)
VALUES ('Nhà Sách Đất Việt', '23-25 Hoàng Hoa Thám, P. 13, Q. Tân Bình', '', '02862972356', 0, 10000000, '', '');
INSERT INTO Daily ( tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai)
VALUES ( 'Nhà Sách 19', '64B Nguyễn Thị Minh Khai, P.ĐaKao, Q.1', '', '02838243913', 0, 10000000, '', '');
INSERT INTO Daily ( tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai)
VALUES ( 'Nhà Sách An Dương Vương', '87/1 Trần Phú, P.4, Q.5', '', '02838351939', 0, 10000000, '', '');
INSERT INTO Daily ( tendl, diachi, email, sdt, tongno, notoida, ngaymuacuoi, trangthai)
VALUES ( 'Nhà Sách Ngoại Văn BOA', 'Phòng C26, Lầu 2, 42 Trần Cao Vân, Q.3', '', '0909892312', 0, 10000000, '', '');
--NXB
CREATE TABLE NXB(
   manxb int IDENTITY(1,1) primary key,
   tennxb NVARCHAR (50) NOT NULL,
   diachi NVARCHAR (250),
   email NVARCHAR (255),
   sdt nvarchar(15),
   sotk nvarchar(35),
   tongno int not null,
   ngaynhapcuoi datetime,
   trangthai bit,
);

INSERT INTO NXB ( tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai)
VALUES ( N'Thanh niên', N'64 Bà Triệu, Hoàn Kiếm, Hà Nội', '', '0462631720', '', 0, '', '');
INSERT INTO NXB ( tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai)
VALUES ( N'Hội Nhà Văn', N'65 Nguyễn Du, Hai Bà Trưng, Hà Nội', '', '04838297915', '', 0, '', '');
INSERT INTO NXB ( tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai)
VALUES ( N'Hà Nội', N'Số 4 - Tống Duy Tân, Hà Nội', 'nxbhanoi@nxbhanoi.com.vn', '0438252916', '', 0, '', '');
INSERT INTO NXB ( tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai)
VALUES ( N'Tri Thức', N'Tầng 1-Tòa nhà VUSTA-53 Nguyễn Du, Quận Hai Bà Trưng, Hà Nội', 'nxbtrithuc@nxbtrithuc.com.vn', '842439454661', '', 0, '', '');
INSERT INTO NXB ( tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai)
VALUES ( N'Hồng Đức', N'Số A2-261 Thụy Khuê, Tây Hồ, Hà Nội', 'nxbhongduc@hieusach.vn', '084437281306', '', 0, '', '');
insert into NXB (tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai) values (N'NXB Kim Đồng', N'123 Ngô Thì Nhậm Q4 TP Hồ Chí Minh', 'nxbkimdong@gmail.com', '0487592687', '3115636676542', '', '', 0)
insert into NXB (tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai) values (N'NXB Giáo Dục', N'34 Hùng Vương Quận Hải Châu TP Đà Nẵng', 'nxbgiaoduc@gmail.com', '0467836825', '5758627652875', '', '', 0)
insert into NXB (tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai) values (N'NXB Phương Nam', N'65 Tú Sương Q8 TP Hồ Chí Minh', 'nxbphuongnam@gmail.com', '04586765458', '0587658763269', '0', '', 0)
insert into NXB (tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai) values (N'NXB Phụ nữ', N'97 Nguyễn Thông Quận Bình Thạnh TP Hồ Chí Minh', 'nxbphunu@gmail.com', '0498758518', '5847585798435', '', '', 0)
insert into NXB (tennxb, diachi, email, sdt, sotk, tongno, ngaynhapcuoi, trangthai) values (N'NXB Nguyễn Văn Cừ', N'90 Trần Quang Khải Q1 TP Hồ Chí Minh', 'nxbnguyenvancu@gmail.com', '0568762569', '4598759870659', '', '', 0)
--Phieu xuat

create table PhieuXuat(
	maphieu int IDENTITY(1,1) primary key,
	ngaylap datetime not null,
	madl int not null,
	nguoigiao nvarchar(50),
	tongtien int not null,	
	trangthai bit
)
--CT phieu xuat
create table CT_PX(
	mapxuat int not null,
	masach int not null,
	soluong int not null, 
	gia int not null,
	thanhtien int not null,
	ghichu nvarchar(255),
	trangthai bit
	
	
)

--Phieu nhap
create table PhieuNhap(
	maphieu int IDENTITY(1,1)  primary key,
	ngaylap datetime not null,
	manxb int not null,
	nguoigiao nvarchar(50),
	tongtien int not null,	
	trangthai bit
)
--CT phieu nhập
create table CT_PN(
	mapnhap int not null,
	masach int not null,
	soluong int not null, 
	gianhap int not null,
	thanhtien int not null,
	ghichu nvarchar(255),
	trangthai bit
)

--Tonkho
create table Tonkho(
	stt int IDENTITY(1,1)  primary key,
	masach int not null,
	soluong int not null,
	ngayupdatetime datetime,
)

--Phiếu thu
--drop table PhieuThu
create table PhieuThu(
	maphieu int IDENTITY(1,1)  primary key,
	madl int not null,
	tongthu int not null,	
	ngaylap datetime not null,
	trangthai bit
)
--CT phieu thu
create table CT_PT(
	mapthu int not null,
	masach int not null,
	soluong int not null, 
	giaxuat int not null,
	thanhtien int not null,
)

--Phiếu chi
--drop table PhieuChi
create table PhieuChi(
	maphieu int IDENTITY(1,1)  primary key,
	manxb int not null,
	tongchi int not null,	
	ngaylap datetime not null,
	trangthai bit
)
--CT phieu chi
--drop table CT_PC
create table CT_PC(
	mapchi int not null,
	masach int not null,
	soluong int not null, 
	gianhap int not null,
	thanhtien int not null,
)

create table Tonkho_DL(
stt int IDENTITY(1,1)  primary key,
	madl int not null,
	masach int not null,
	soluong int not null,
)
create table CongNo_DL(
	stt int IDENTITY(1,1)  primary key,
	madl int not null,
	ngay datetime not null,
	tongno int not null,
)
create table CongNo_NXB(
stt int IDENTITY(1,1)  primary key,
	manxb int not null,
	ngay datetime not null,
	tongno int not null,
)
ALTER TABLE CongNo_NXB ADD FOREIGN KEY(manxb) REFERENCES NXB(manxb);
ALTER TABLE CongNo_DL ADD FOREIGN KEY(madl) REFERENCES Daily(madl);
ALTER TABLE Tonkho_DL ADD FOREIGN KEY(madl) REFERENCES Daily(madl);
ALTER TABLE Tonkho_DL ADD FOREIGN KEY(masach) REFERENCES Sach(masach);

ALTER TABLE Sach ADD FOREIGN KEY(manxb) REFERENCES NXB(manxb);
ALTER TABLE PhieuXuat ADD FOREIGN KEY(madl) REFERENCES Daily(madl);
ALTER TABLE PhieuThu ADD FOREIGN KEY(madl) REFERENCES Daily(madl);
ALTER TABLE Tonkho ADD FOREIGN KEY(masach) REFERENCES Sach(masach);
ALTER TABLE CT_PC ADD FOREIGN KEY(mapchi) REFERENCES PhieuChi(maphieu);
ALTER TABLE CT_PT ADD FOREIGN KEY(mapthu) REFERENCES PhieuThu(maphieu);
ALTER TABLE CT_PN ADD FOREIGN KEY(mapnhap) REFERENCES PhieuNhap(maphieu);
ALTER TABLE CT_PX ADD FOREIGN KEY(mapxuat) REFERENCES PhieuXuat(maphieu);
ALTER TABLE CT_PC ADD FOREIGN KEY(masach) REFERENCES Sach(masach);
ALTER TABLE CT_PT ADD FOREIGN KEY(masach) REFERENCES Sach(masach);
ALTER TABLE CT_PN ADD FOREIGN KEY(masach) REFERENCES Sach(masach);
ALTER TABLE CT_PX ADD FOREIGN KEY(masach) REFERENCES Sach(masach);
ALTER TABLE PhieuNhap ADD FOREIGN KEY(manxb) REFERENCES NXB(manxb);
ALTER TABLE PhieuChi ADD FOREIGN KEY(manxb) REFERENCES NXB(manxb);
select tensach ,soluong from Sach,CT_PN where Sach.masach= CT_PN.masach

